﻿using System;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Threading;

namespace Main1
{
    class Program
    {
        public static MethodInfo method;
        public static object obj;

        public static void ShowInterface()
        {
            Console.WriteLine("Connected Interface DLL");
            Assembly second = Assembly.LoadFrom("Calculation.dll");
            Console.Write("Elements in array: ");
            int N = int.Parse(Console.ReadLine());
            Console.WriteLine("DLL INFO: " + second.FullName);
            Type t = second.GetType("Calculation.Calculation", true, true);
            obj = Activator.CreateInstance(t);
            method = t.GetMethod("callQSort");
            Object result = method.Invoke(obj, new object[] { N });
        }

        public static void Main(string[] args)
        {
            Assembly second = Assembly.LoadFrom("interface.dll");
            Type t = second.GetType("interface.interface", true, true);

            method = t.GetMethod("ShowInterface");
            Object result = method.Invoke(obj, new object[] { });
        }
    }
}
