﻿using System;

namespace Task
{
    class Program
    {
        public static int partition(int[] array, int start, int end)
        {
            int temp;
            int marker = start;
            for (int i = start; i <= end; i++)
            {
                if (array[i] < array[end])
                {
                    temp = array[marker];
                    array[marker] = array[i];
                    array[i] = temp;
                    marker += 1;
                }
            }

            temp = array[marker];
            array[marker] = array[end];
            array[end] = temp;
            return marker;
        }

        public static void quicksort(int[] array, int start, int end)
        {
            if (start >= end)
            {
                return;
            }

            int pivot = partition(array, start, end);
            quicksort(array, start, pivot - 1);
            quicksort(array, pivot + 1, end);
        }


        public static void callQSort(int N)
        {
            Random rnd = new Random(DateTime.Now.Millisecond);
            int[] array = new int[N];
            for (int i = 0; i < N; i++)
            {
                int i_rnd = rnd.Next(0, 100);
                array[i] = i_rnd;
                Console.Write($" {array[i]}\t");
            }
            int a = 1000;
            quicksort(array, 0, array.Length - 1);
            Console.WriteLine("\nSort array:");
            for (int i = 0; i < N; i++)
            {
                Console.Write($" {array[i]}\t");
                Console.Write(Math.Sin(a));
            }
        }
    }
}
